# Gauge

Eine fernsteuerbare Anzeige für beliebige Werte (Netzwerkdurchsatz, Stromverbrauch...) via MQTT auf Basis eines EtherTen (Arduino+Ethernet)

How to get it running
=====

 * Servomotor anschließen (Braun=GND, Mitte=5V, Orange=Steuerleitung(D9/PWM))
 * Arduino ans ein Netz anschließen, welches DHCP kann.
 * Unbedingt Stromversorgung (z.B. 9V 1.3A) anschließen
 * Programm (gauge/gauge.ino) Flashen

Auf dem Status channel meldet sich der Arduino mit seiner IP und dass er bereit ist (ggf. mqtt Server abändern)

    $ mosquitto_sub -h test.mosquitto.org -t "gauge/status" -v

    gauge/status Setup done. My IP is:
    gauge/status 192.168.1.203
    gauge/status subscribing to gauge/input now...

Nun z.B. einen Testlauf starten, der Motor sollte Geräusche machen

    $ ./gauge/gauge_testlauf.sh

