#!/bin/bash

mosquitto_pub -h test.mosquitto.org -t 'gauge/angle_limit' -m "160"
mosquitto_pub -h test.mosquitto.org -t 'gauge/scale_limit' -m "100"

while true; do
	for i in `seq 0 100`; do
		mosquitto_pub -h test.mosquitto.org -t 'gauge/input' -m $i

	done
done
