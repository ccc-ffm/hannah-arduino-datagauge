#include <SPI.h>
#include <Ethernet.h>
#include <PubSubClient.h>
#include <Servo.h> 

Servo myservo;  // create servo object to control a servo 
                // a maximum of eight servo objects can be created 
int pos = 0;    // variable to store the servo position 

byte mac[]    = {  0xCA, 0xFE, 0xBA, 0xBE, 0x13, 0x37 };
byte server[] = { 85,119,83,194 }; // MQTT server IP

int scale_limit = -1; // Ende der Skala (z.B. 100 bei 0..100Mbit/s - hängt von Beschriftung ab)
int angle_limit = -1; // Winkel ab dem die Nadel anschlägt (bewegt sich von 0..angle_limit, muss kleiner 360° sein)

/*
 Beispielrechnung:
 
 scale_limit = 100Mbit/s
 es fließen 50Mbit/s (data)
 angle_limit = 160°
 
 => (angle_limit/scale_limit) * data = 80° (Mitte) :)
 
*/

EthernetClient ethClient;
PubSubClient client(server, 1883, callback, ethClient);

void callback(char* topic, byte* payload, unsigned int length) {
  // IMMEDIATELY copy the payload since it is lost otherwise on any publish or receive! 
  // YOU SHOULDN'T DO ANY PUBLISH INSIDE CALLBACK, THIS LEADS INTO A HANGUP
  byte rawdata[length];
  memcpy(rawdata, payload, length);

  int data=0;
  // convert payload to int
  Serial.println(length);

  for(int i=0;i<length; i++){
      Serial.print(rawdata[i]);
      data += (rawdata[i]-48) * pow(10, (length-i)-1);
  }
  Serial.println("");
  Serial.println(data);

  
  Serial.println(topic);
  
  if(strcmp(topic, "gauge/input")==0){
    if(angle_limit <0 || scale_limit<0){
        Serial.println("I am unconfigured!");
        return;
    }
    Serial.println("Got input message");
    int display_value = (angle_limit/scale_limit)*data; // calculate output angle
    Serial.println(display_value);
    
    myservo.write(display_value);    // tell servo to go to position in variable 'pos' 
    delay(15);                       // waits 15ms for the servo to reach the position 
  } else if(strcmp(topic, "gauge/scale_limit")==0){
     Serial.println("Got configuration message for scale_limit");
      scale_limit = data;
  } else if(strcmp(topic, "gauge/angle_limit")==0){
      Serial.println("Got configuration message for angle_limit");
      angle_limit = data;
  }
   Serial.println("message processed");
}

void setup() {
  myservo.attach(9);  // attaches the servo on pin 9 to the servo object 
  Serial.begin(9600);
  
  // start the Ethernet connection:
  if (Ethernet.begin(mac) == 0) {
    Serial.println("Failed to configure Ethernet using DHCP, falling back to static IP 192.168.2.252");
    byte fallback_ip[] = { 192, 168, 1, 252 };
    Ethernet.begin(mac, fallback_ip);
  } else {
    Serial.println(Ethernet.localIP());
  }
    
  while (!client.connect("arduinoClient")); // try connecting until it succeeds
    
    client.publish("gauge/status","Setup done. My IP is:");
    char ip_buf[15] = "";
    ipaddrToChar(Ethernet.localIP(), ip_buf, 15);
    client.publish("gauge/status", ip_buf);
    client.publish("gauge/status","subscribing to gauge/input now...");
    client.subscribe("gauge/input");
    client.subscribe("gauge/angle_limit");
    client.subscribe("gauge/scale_limit");
}

void loop()
{
  client.loop();
}

void ipaddrToChar(IPAddress address, char* ip_buf, int len) {
  String ip = String(address[0]) + "." + 
  String(address[1]) + "." + 
  String(address[2]) + "." + 
  String(address[3]);
  ip.toCharArray(ip_buf, len);
}
